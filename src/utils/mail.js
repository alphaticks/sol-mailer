import * as ed25519 from '@noble/ed25519'
import {createHash} from 'crypto'
import CryptoJS from 'crypto-js'
import * as bip39 from 'bip39'
import {bytesToHex, hexToBytes, makeRequest, publicKeyToIPNS} from './utils'

//const crypto = require('libp2p-crypto');
//const ced25519 = crypto.keys.supportedKeys.ed25519

class MailBox {
  constructor (ID, connector, ipfs, storage, password, seed) {
    // Need to create seed phrase
    this.ID = ID
    this.ipfs = ipfs
    this.storage = new StorageHandler(storage, password)
    this.password = password
    this.seed = seed
    this.contacts = {}
    this.requests = {}
    this.connector = connector
  }

  async load () {
    // TODO load or generate private key
    // Generate seed phrase, or check in storage if seed phrase exists
    let isOldSeed = !!this.storage.getDecrypted('seed') || !!this.seed
    this.seed = this.seed || this.storage.getDecrypted('seed') || bip39.generateMnemonic()
    this.privateKey = createHash('sha256').update(this.seed).digest('hex')
    this.publicKey = await ed25519.getPublicKey(this.privateKey)
    let contacts = this.storage.getDecrypted('contacts') || []
    for (let k in contacts) {
      let ID = contacts[k]
      console.log("LOADING", ID)
      this.contacts[ID] = new Contact(ID, this.storage, this.ipfs)
      this.contacts[ID].load()
    }
    await this.save()
    return [isOldSeed, this.seed]
  }

  async fetchRequests () {
    console.log("FETCH REQUESTS")
    let txs = await this.connector.getFriendRequests(this.ID, 0)
    txs.reverse()
    for (let tx of txs) {
      console.log(tx)
      if (tx.sender === this.ID) {
        // We sent the request
        let contact = this.addContactRequest(tx.recipient)
        console.log("CONTACT REQUEST ADDED", tx.recipient)
        //TODO change the below in order to only add the contact if tx.public===this.publicKey
        // if (tx.publicKey.equals(this.publicKey)) {
          contact.setRequestID(tx.requestID)
        // }
      } else {
        // We got the request
        let contactID = this.ID + ":" + tx.sender
        if (contactID in this.contacts) {
          console.log("MATCHING CONTACT ID", contactID)
          //let contact = this.contacts[tx.sender]
          await this.addContact(tx.sender, tx.publicKey)
        } else if (tx.sender !== this.ID) {
          console.log("NO MATCHING CONTACT ID", contactID)
          this.requests[tx.sender] = tx
        }
      }
    }
    console.log(this.contacts)
  }

  getRequests () {
    let requests = []
    for (let k in this.requests) {
      requests.push(this.requests[k])
    }
    return requests
  }

  async save () {
    this.storage.setEncrypted('seed', this.seed)
    let contacts = []
    for (let k in this.contacts) {
      contacts.push(k)
      await this.contacts[k].save()
    }
    this.storage.setEncrypted('contacts', contacts)
  }

  getContact (ID) {
    return this.contacts[ID]
  }

  getContacts () {
    let contacts = []
    for (let k in this.contacts) {
      contacts.push(this.contacts[k])
    }
    return contacts
  }

  addContactRequest (ID) {
    // Create a contact with no public key
    let contactID = this.ID + ":" + ID
    if (!(contactID in this.contacts)) {
      this.contacts[contactID] = new Contact(contactID, this.storage, this.ipfs)
      this.save()
    }
    let request = this.requests[ID]
    if (request) {
      delete this.requests[ID]
    }
    return this.contacts[contactID]
  }

  async acceptContactRequest (ID) {
    let request = this.requests[ID]
    if (!request) {
      return
    }
    await this.addContact(ID, request.publicKey)
  }

  // Add contact can be someone we don't have public key of ? how
  async addContact (ID, publicKey) {
    // TODO only for tests
    if (publicKey === null) {
      const privateKey = ed25519.utils.randomPrivateKey()
      publicKey = await ed25519.getPublicKey(privateKey)
    }
    let contact = this.addContactRequest(ID)
    // You get a transaction from someone, you have their ID (address) and public key (ed255..)
    // You store it in your contact list
    // Compute the shared secret

    let sharedSecret
    try {
      sharedSecret = await ed25519.getSharedSecret(this.privateKey, publicKey)
    } catch (err) {
      return null
    }

    console.log('SHARED SECRET', sharedSecret, this.ID)

    // eslint-disable-next-line no-undef
    let inboxPrivateKey = createHash('sha256').update(sharedSecret).update(this.ID).digest('hex')
    let outboxPrivateKey = createHash('sha256').update(sharedSecret).update(ID).digest('hex')

    // Derive the inbox IPNS address
    let inboxPublicKey = await ed25519.getPublicKey(inboxPrivateKey)
    let outboxPublicKey = await ed25519.getPublicKey(outboxPrivateKey)
    console.log('inbox public key', inboxPublicKey)
    console.log('outbox public key', outboxPublicKey)
    await contact.setContactInfo(publicKey, outboxPrivateKey, inboxPublicKey)
    return contact
  }
}

class Contact {
  constructor (ID, storage, ipfs) {
    this.ID = ID
    this.storage = storage
    this.ipfs = ipfs
    this.requestID = null
    this.publicKey = null
    this.inboxPublicKey = null
    this.outboxPrivateKey = null
    this.inboxIndex = {messages: [], time: new Date('0-0-0')}
    this.outboxIndex = {messages: [], time: new Date('0-0-0')}
    this.messages = []
  }

  save () {
    let state = {
      requestID: this.requestID,
      publicKey: bytesToHex(this.publicKey),
      inboxPublicKey: bytesToHex(this.inboxPublicKey),
      outboxPrivateKey: this.outboxPrivateKey,
      inboxIndex: this.inboxIndex,
      outboxIndex: this.outboxIndex,
    }
    this.storage.setEncrypted(this.ID, state)
  }

  async load () {
    let state = this.storage.getDecrypted(this.ID)
    if (state !== null) {
      this.requestID = state.requestID
      this.publicKey = hexToBytes(state.publicKey)
      this.inboxPublicKey = hexToBytes(state.inboxPublicKey)
      this.outboxPrivateKey = state.outboxPrivateKey
      this.inboxIndex = state.inboxIndex
      this.outboxIndex = state.outboxIndex
      this.updateMessages()
      console.log(this.outboxIndex)
      //let outboxPublicKey = await ed25519.getPublicKey(this.outboxPrivateKey)
      //console.log("outpk", outboxPublicKey)
      console.log('inpk', this.inboxPublicKey)
      this.syncIPFS()
      return true
    } else {
      return false
    }
  }

  async syncIPFS () {
    if (!this.outboxPrivateKey) {
      return
    }
    let inIPFS = false
    let keys = await this.ipfs.key.list()
    for (let i = 0; i < keys.length; i++) {
      if (keys[i].name === this.ID) {
        this.ipfs.key.rm(this.ID)
      }
    }

    if (!inIPFS) {
      let b = Buffer.from(`302E020100300506032B657004220420${this.outboxPrivateKey}`, 'hex').toString('base64')
      let pem = `-----BEGIN PRIVATE KEY-----
${b}
-----END PRIVATE KEY-----`
      let formData = new FormData()
      formData.append('key', pem)
      await makeRequest('POST', `http://78.47.93.35:5001/api/v0/key/import?arg=${this.ID}&format=pem-pkcs8-cleartext`, formData)
    }

    let outboxPublicKey = await ed25519.getPublicKey(this.outboxPrivateKey)
    let outboxIPNS = await publicKeyToIPNS(outboxPublicKey)
    let outboxResolved = []
    for await (const a of this.ipfs.name.resolve(outboxIPNS, {timeout: 1000})) {
      outboxResolved.push(a.split('/')[2])
    }
    let outboxIndex
    for (const r of outboxResolved) {
      for await (const msg of this.ipfs.cat(r, {timeout: 1000})) {
        let b = Buffer.from(msg)
        outboxIndex = JSON.parse(b)
      }
    }

    if (outboxIndex) {
      console.log(outboxIndex, this.outboxIndex)
      // Compare our index with the one from IPFS
      if (!this.outboxIndex.time || outboxIndex.time > this.outboxIndex.time) {
        console.log("OUTBOX INDEX UPDATED")
        this.outboxIndex = outboxIndex
      }
    }

    let inboxIPNS = await publicKeyToIPNS(this.inboxPublicKey)
    console.log(inboxIPNS, "INBOX INDEX")
    let inboxResolved = []
    for await (const a of this.ipfs.name.resolve(inboxIPNS, {timeout: 1000})) {
      inboxResolved.push(a.split('/')[2])
    }
    let inboxIndex
    for (const r of inboxResolved) {
      for await (const msg of this.ipfs.cat(r, {timeout: 1000})) {
        let b = Buffer.from(msg)
        inboxIndex = JSON.parse(b)
      }
    }

    console.log(inboxIndex)
    if (inboxIndex) {
      // Compare our index with the one from IPFS
      if (!this.inboxIndex.time ||inboxIndex.time > this.inboxIndex.time) {
        console.log("INBOX INDEX UPDATED")
        this.inboxIndex = inboxIndex
      }
    }
    this.updateMessages()
  }

  updateMessages() {
    let messages = []
    for (let i in this.inboxIndex.messages) {
      let m = this.inboxIndex.messages[i]
      messages.push({time: new Date(m.time), cid: m.cid, me: false})
    }
    for (let i in this.outboxIndex.messages) {
      let m = this.outboxIndex.messages[i]
      messages.push({time: new Date(m.time), cid: m.cid, me: true})
    }

    messages.sort((a, b) => {
      if (a.time < b.time) {
        return 1
      } else {
        return -1
      }
    })
    console.log(messages)
    this.messages = messages
  }

  async setContactInfo (publicKey, outboxPrivateKey, inboxPublicKey) {
    this.publicKey = publicKey
    this.outboxPrivateKey = outboxPrivateKey
    this.inboxPublicKey = inboxPublicKey
    this.syncIPFS()
    this.save()
  }

  setRequestID (ID) {
    // The tx ID
    this.requestID = ID
    this.save()
  }

  async send (msg) {
    // Need to post the message to ipfs, then update the entry list, then point to it in ipns
    const result = await this.ipfs.add(msg)
    this.outboxIndex.messages.unshift({cid: result.cid.toString(), time:(new Date()).toString()})
    this.outboxIndex.time = new Date()
    this.messages.unshift({cid: result.cid.toString(), time:(new Date()).toString(), me:true})
    let string = JSON.stringify(this.outboxIndex)
    const outboxIndexCID = await this.ipfs.add(string)
    let res = await this.ipfs.name.publish(outboxIndexCID.cid, {'key': this.ID})
    console.log('published', res)
    this.save()
  }
}

class StorageHandler {
  constructor (storage, password) {
    this.storage = storage
    this.salt = this.getSalt()
    this.iv = this.getIv()
    this.checkPassword(password)
    this.setPassword(password)
    this.password = password
    this.storage.setItem('salt', this.salt.toString())
    this.storage.setItem('iv', this.iv.toString())
  }

  set (key, obj) {
    let string = JSON.stringify(obj)
    this.storage.setItem(key, string)
  }

  get (key) {
    let string = this.storage.getItem(key)
    let obj = null
    if (string != null) {
      obj = JSON.parse(string)
    }
    return obj
  }

  setEncrypted (key, obj) {
    let encrypted = this.cypher(obj)
    this.storage.setItem(key, encrypted)
  }

  getDecrypted (key) {
    let string = this.storage.getItem(key)
    let obj = null
    if (string != null) {
      let decrypted = this.decypher(string)
      obj = JSON.parse(decrypted)
    }
    return obj
  }

  cypher (obj) {
    let str = JSON.stringify(obj)
    let key128Bits = CryptoJS.PBKDF2(this.password, this.salt, {
      keySize: 128 / 32
    })
    let iv = CryptoJS.enc.Hex.parse(this.iv.toString())
    let result = CryptoJS.lib.SerializableCipher.encrypt(CryptoJS.algo.AES, str, key128Bits, {iv: iv})
    return result.toString()
  }

  decypher (ciphertext) {
    let res = CryptoJS.lib.SerializableCipher._parse(ciphertext, CryptoJS.enc.Base64)
    let key128Bits = CryptoJS.PBKDF2(this.password, this.salt, {
      keySize: 128 / 32
    })
    let iv = CryptoJS.enc.Hex.parse(this.iv.toString())
    let cypherParams = CryptoJS.lib.CipherParams.create({
      ciphertext: res,
      key: key128Bits,
      iv: iv,
      algorithm: CryptoJS.algo.AES,
    })
    let word = CryptoJS.lib.SerializableCipher.decrypt(CryptoJS.algo.AES, cypherParams, key128Bits, {iv: iv})
    let objString = CryptoJS.enc.Latin1.stringify(word)
    return objString
  }

  setPassword (password) {
    let key128Bits = CryptoJS.PBKDF2(password, this.salt, {
      keySize: 128 / 32
    })
    let keyHash = CryptoJS.SHA256(key128Bits)
    this.set('password', keyHash.toString())
  }

  checkPassword (password) {
    let pass = this.get('password')
    if (pass !== null) {
      let key128Bits = CryptoJS.PBKDF2(password, this.salt, {
        keySize: 128 / 32
      })
      let keyHash = CryptoJS.SHA256(key128Bits)
      if (keyHash.toString() !== pass) {
        throw {name: 'IncorrectPasswordError', message: `got ${pass} instead of ${keyHash}`}
      }
    }
  }

  getIv () {
    let string = this.storage.getItem('iv')
    let obj
    if (string != null) {
      obj = CryptoJS.enc.Hex.parse(string)
    } else {
      obj = CryptoJS.lib.WordArray.random(128 / 8)
    }
    return obj
  }

  getSalt () {
    let string = this.storage.getItem('salt')
    let obj
    if (string != null) {
      obj = CryptoJS.enc.Hex.parse(string)
    } else {
      obj = CryptoJS.lib.WordArray.random(128 / 8)
    }
    return obj
  }

}

export {MailBox}
