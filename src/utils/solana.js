const web3 = require("@solana/web3.js")

class SolanaHandler {
    constructor(wallet){
        this.connection = new web3.Connection(web3.clusterApiUrl('mainnet-beta'), 'confirmed')
        this.wallet = wallet
    }

    getWallet() {
        return this.wallet
    }

    async getFriendRequests(address, from) {
        let transactions = await this.getAccountTransactions(address.replace("sol-", ""), from)
        let requests = []
        for (let tx of transactions) {
            if (tx.memo != null) {
                let split = tx.memo.split("]")[1]
                if (split.slice(1, 15) === "sol-mailer.xyz") {
                    let txInfo = await this.connection.getTransaction(tx.signature)
                    if (txInfo.transaction.message.accountKeys.length === 4) {
                        let sender = txInfo.transaction.message.accountKeys[0]
                        let add = Buffer.from(split.slice(16), 'base64')
                        if (add.length === 32) {
                            console.log(txInfo.transaction)
                            requests.push({
                                sender: "sol-" + sender,
                                recipient: "sol-" + txInfo.transaction.message.accountKeys[1],
                                publicKey: add,
                                requestID: tx.signature
                            })
                        }
                    }
                }
            }
        }
        return requests
    }

    async getAccountTransactions(address, from) {
        let publicKey = new web3.PublicKey(address)
        let res = await this.connection.getSignaturesForAddress(publicKey)
        let transactions = []
        for (let tx of res) {
            if (tx.slot > from) {
                transactions.push(tx)
            } else {
                break
            }
        }
        while (res.length > 0 && res[res.length - 1].slot > from) {
            res = await this.connection.getSignaturesForAddress(publicKey, {before: res[res.length-1].signature})
            for (let tx in res) {
                if (tx.slot > from) {
                    transactions.push(tx)
                } else {
                    break
                }
            }
        }
        return transactions
    }

    async contactRequest (to, memo) {
        const destPublicKey = new web3.PublicKey(to)
        const instructions = []
        const enc = new TextEncoder()
        const memoInstruction = new web3.TransactionInstruction({
            keys: [],
            programId: new web3.PublicKey('MemoSq4gqABAXKb96qnH8TysNcWxMyWCqXgDLGmfcHr'),
            data: enc.encode(memo)
        })
        instructions.push(memoInstruction)
        const dustInstruction = web3.SystemProgram.transfer({
            fromPubkey: this.wallet.publicKey,
            toPubkey: destPublicKey,
            lamports: 1,
        })

        instructions.push(dustInstruction)

        console.log(memoInstruction, memo)

        const transaction = new web3.Transaction().add(...instructions)
        transaction.feePayer = this.wallet.publicKey
        transaction.recentBlockhash = (await this.connection.getRecentBlockhash()).blockhash
        return await this.wallet.sendTransaction(transaction)
    }
}
export {SolanaHandler}
