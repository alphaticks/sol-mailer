import {createHash} from 'crypto'
import crypto from 'libp2p-crypto'
import {createFromPubKey} from 'peer-id'
/* global BigInt */

const hexes = Array.from({length: 256}, (v, i) => i.toString(16).padStart(2, '0'))

function bytesToHex (uint8a) {
  if (uint8a === null) {
    return null
  }
  // pre-caching improves the speed 6x
  if (!(uint8a instanceof Uint8Array)) throw new Error('Uint8Array expected')
  let hex = ''
  for (let i = 0; i < uint8a.length; i++) {
    hex += hexes[uint8a[i]]
  }
  return hex
}

function bytesToNumberLE (uint8a) {
  if (!(uint8a instanceof Uint8Array)) throw new Error('Expected Uint8Array')
  // eslint-disable-next-line no-undef
  return BigInt('0x' + bytesToHex(Uint8Array.from(uint8a).reverse()))
}

// Caching slows it down 2-3x
function hexToBytes (hex) {
  if (hex === null) {
    return null
  }
  if (typeof hex !== 'string') {
    throw new TypeError('hexToBytes: expected string, got ' + typeof hex)
  }
  if (hex.length % 2) throw new Error('hexToBytes: received invalid unpadded hex')
  const array = new Uint8Array(hex.length / 2)
  for (let i = 0; i < array.length; i++) {
    const j = i * 2
    const hexByte = hex.slice(j, j + 2)
    const byte = Number.parseInt(hexByte, 16)
    if (Number.isNaN(byte) || byte < 0) throw new Error('Invalid byte sequence')
    array[i] = byte
  }
  return array
}

function adjustBytes25519 (bytes) {
  // Section 5: For X25519, in order to decode 32 random bytes as an integer scalar,
  // set the three least significant bits of the first byte
  bytes[0] &= 248 // 0b1111_1000
  // and the most significant bit of the last to zero,
  bytes[31] &= 127 // 0b0111_1111
  // set the second most significant bit of the last byte to 1
  bytes[31] |= 64 // 0b0100_0000
  return bytes
}

function ensureBytes (hex, expectedLength) {
  // Uint8Array.from() instead of hash.slice() because node.js Buffer
  // is instance of Uint8Array, and its slice() creates **mutable** copy
  const bytes = hex instanceof Uint8Array ? Uint8Array.from(hex) : hexToBytes(hex)
  if (typeof expectedLength === 'number' && bytes.length !== expectedLength) {
    throw new Error(`Expected ${expectedLength} bytes`)
  }
  return bytes
}

function decodeScalar25519 (n) {
  // and, finally, decode as little-endian.
  // This means that the resulting integer is of the form 2 ^ 254 plus eight times a value between 0 and 2 ^ 251 - 1(inclusive).
  return bytesToNumberLE(adjustBytes25519(ensureBytes(n, 32)))
}

function encodeScalar25519 (n) {
  return numberTo32BytesLE(n)
}

function numberTo32BytesBE(num) {
  const hex = num.toString(16).padStart(32 * 2, '0')
  return hexToBytes(hex)
}

function numberTo32BytesLE(num) {
  return numberTo32BytesBE(num).reverse();
}

function makeRequest (method, url, body) {
  return new Promise(function (resolve, reject) {
    var xhr = new XMLHttpRequest();
    xhr.open(method, url);
    xhr.onload = function () {
      if (xhr.status >= 200 && xhr.status < 300) {
        resolve(xhr.response);
      } else {
        reject({
          status: xhr.status,
          statusText: xhr.statusText
        });
      }
    };
    xhr.onerror = function () {
      reject({
        status: xhr.status,
        statusText: xhr.statusText
      });
    };
    xhr.send(body);
  });
}

function getPrivateScalar (privateKey) {
  let bytes = ensureBytes(privateKey, 32)
  const hashed = createHash('sha512').update(bytes).digest()
  // First 32 bytes of 64b uniformingly random input are taken,
  // clears 3 bits of it to produce a random field element.
  return adjustBytes25519(hashed.slice(0, 32));
  // Second 32 bytes is called key prefix (5.1.6)
  // The actual private scalar
  //return ed25519.utils.mod(bytesToNumberLE(head), ed25519.CURVE.l)
}

function bufferToBigInt(buf) {
  const hex = [];
  const u8 = Uint8Array.from(buf);

  u8.forEach(function (i) {
    var h = i.toString(16);
    if (h.length % 2) { h = '0' + h; }
    hex.push(h);
  });

  return BigInt('0x' + hex.join(''));
}

async function publicKeyToIPNS(publicKey) {
  let p2pPubKeyOut = new crypto.keys.supportedKeys.ed25519.Ed25519PublicKey(publicKey)
  let peerIDOut = await createFromPubKey(p2pPubKeyOut.bytes)
  let bufOut = Buffer.from(peerIDOut._id).toString('hex')
  bufOut = '0172' + bufOut
  let bufferOut = Buffer.from(bufOut, 'hex')
  let numOut = bufferToBigInt(bufferOut)
  return 'k' + numOut.toString(36)
}

export {publicKeyToIPNS, bufferToBigInt, getPrivateScalar, makeRequest, decodeScalar25519, encodeScalar25519, bytesToHex, hexToBytes, numberTo32BytesBE, numberTo32BytesLE}

