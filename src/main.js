import Vue from 'vue'
import App from './App'
import Home from './pages/Home'
import vueCountryRegionSelect from 'vue-country-region-select'
import VueRouter from 'vue-router'
import '../node_modules/bulma/css/bulma.css'
import {create} from 'ipfs-http-client'


Vue.config.productionTip = false
Vue.use(VueRouter)
Vue.use(vueCountryRegionSelect)
Vue.use(require('vue-cookies'))

Vue.prototype.$ipfsURL = 'http://78.47.93.35:5001'
Vue.prototype.$ipfs = create('http://78.47.93.35:5001')
// 2. Define some routes
// Each route should map to a component. The "component" can
// either be an actual component constructor created via
// `Vue.extend()`, or just a component options object.
// We'll talk about nested routes later.
const routes = [
  { path: '/', component: Home },
]

// 3. Create the router instance and pass the `routes` option
// You can pass in additional options here, but let's
// keep it simple for now.
const router = new VueRouter({
  routes // short for `routes: routes`
})

/* -disable no-new */
new Vue({ el: '#app', router, render: h => h(App) })
